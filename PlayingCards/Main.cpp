//Brett Davis
//Playing Cards

//Edited 9/11/19
//By: Kathy Xiong

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit
{
	Hearts, Diamonds, Clubs, Spades
};

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

struct Card
{
	Suit suit;
	Rank rank;
};


void PrintCard(Card card)
{
	cout << "The ";

	switch (card.rank)
	{
	case Ace: cout << "Ace of "; break;
	case King: cout << "King of "; break;
	case Queen: cout << "Queen of "; break;
	case Jack: cout << "Jack of "; break;
	case Ten: cout << "Ten of "; break;
	case Nine: cout << "Nine of "; break;
	case Eight: cout << "Eight of "; break;
	case Seven: cout << "Seven of "; break;
	case Six: cout << "Six of "; break;
	case Five: cout << "Five of "; break;
	case Four: cout << "Four of "; break;
	case Three: cout << "Three of "; break;
	case Two: cout << "Two of "; break;

	}


	switch (card.suit)
	{
	case Spades: cout << "Spades"; break;
	case Hearts: cout << "Hearts"; break;
	case Clubs: cout << "Clubs"; break;
	case Diamonds: cout << "Diamonds"; break; 
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		cout << card1.rank;
	}
	else
	{
		cout << card2.rank;
	}
}

int main()
{

	_getch();
	return 0;
}